Práctica 01

Reproductor musical para la materia de Computación Concurrente
del semestre 2017-1 de la Facultad de Ciencias.

Hace uso de las bibliotecas
    - JLayer 1.0.1
    - Java ID3 Tag 0.5.4
    
Elaborado por:
    - Fernando Antonio Sánchez Montoya
    - Jesús Vila Sánchez