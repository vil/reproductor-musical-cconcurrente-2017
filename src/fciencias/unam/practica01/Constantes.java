/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fciencias.unam.practica01;

/**
 * Pr&aacute;ctica 01
 * Interface que contiene las constante que se manejan en las otras clases.
 * @author Jes&uacute;s Vila S&aacute;nchez, 309280970
 * @author Fernando Antonio S&aacute;nchez Montoya, 309160568
 */
public interface Constantes {
    
    final int COLUMNA_CANCION = 1;
    final int RENGLON_DEFAULT = 0;
    
    final int MAX_RADIO = 50;
    final int MAX_HEIGHT_PANEL = 165;
    final int MAX_WIDTH_PANEL = 800;
    
    final int MAX_LARGO_X = 300;
    final int MAX_ALTO_Y = 150;
    
    final int MAX_VALOR_RGB = 256;
    
}
