/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fciencias.unam.practica01;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;

/**
 * Clase donde se maneja la reproducci&oacute;n y la detenci&oacute;n de la m&uacute;sica
 * @author Jes&uacute;s Vila S&aacute;nchez, 309280970
 * @author Fernando Antonio S&aacute;nchez Montoya, 309160568
 */
public class Reproductor {

    private BufferedInputStream sonido;
    public AdvancedPlayer reproductor;
 
    /**
     * Reproduce el archivo indicado si existe
     * @param archivo Archivo que se quiere reproducir 
     */
    public void reproducir(String archivo) {
        try {
            sonido = new BufferedInputStream(new FileInputStream(archivo));
            reproductor = new AdvancedPlayer(sonido);
            
        } catch (FileNotFoundException | JavaLayerException ex) {
            Logger.getLogger(Reproductor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    reproductor.play();
                } catch (JavaLayerException ex) {
                    Logger.getLogger(Reproductor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        thread.start();
    }
    /**
     * Detiene la m&uacute;sica y limpia la etiqueta donde se indica la canci&oacute;n que se 
     * est&aacute; reproduciendo
     */
    public void detener() {
        if (reproductor != null) {
            ReproductorGUI.labelReproduciendo.setText("");
            reproductor.close();
        }
    }
}
